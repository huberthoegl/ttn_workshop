/* Source:
 * https://raw.githubusercontent.com/mcci-catena/arduino-lmic/master/examples/ttn-otaa/ttn-otaa.ino
 * https://wiki.octoate.de/doku.php/thethingsnetwork:esp32_mit_868_mhz_lora_modul
 */

/*******************************************************************************
 * Copyright (c) 2015 Thomas Telkamp and Matthijs Kooijman
 * Copyright (c) 2018 Terry Moore, MCCI
 *
 * Permission is hereby granted, free of charge, to anyone
 * obtaining a copy of this document and accompanying files,
 * to do whatever they want with them without any restriction,
 * including, but not limited to, copying, modification and redistribution.
 * NO WARRANTY OF ANY KIND IS PROVIDED.
 *
 * This example sends a valid LoRaWAN packet with payload "Hello,
 * world!", using frequency and encryption settings matching those of
 * the The Things Network.
 *
 * This uses OTAA (Over-the-air activation), where where a DevEUI and
 * application key is configured, which are used in an over-the-air
 * activation procedure where a DevAddr and session keys are
 * assigned/generated for use with all further communication.
 *
 * Note: LoRaWAN per sub-band duty-cycle limitation is enforced (1% in
 * g1, 0.1% in g2), but not the TTN fair usage policy (which is probably
 * violated by this sketch when left running for longer)!

 * To use this sketch, first register your application and device with
 * the things network, to set or generate an AppEUI, DevEUI and AppKey.
 * Multiple devices can use the same AppEUI, but each device has its own
 * DevEUI and AppKey.
 *
 * Do not forget to define the radio type correctly in
 * arduino-lmic/project_config/lmic_project_config.h or from your BOARDS.txt.
 *
 *******************************************************************************/

// --hh
#include <Arduino.h>
#include <stdlib.h>


#include <lmic.h>
#include <hal/hal.h>
#include <SPI.h>

// For normal use, we require that you edit the sketch to replace FILLMEIN
// with values assigned by the TTN console. However, for regression tests,
// we want to be able to compile these scripts. The regression tests define
// COMPILE_REGRESSION_TEST, and in that case we define FILLMEIN to a non-
// working but innocuous value.
//
#ifdef COMPILE_REGRESSION_TEST
# define FILLMEIN 0
#else
# warning "You must replace the values marked FILLMEIN with real values from the TTN control panel!"
# define FILLMEIN (#dont edit this, edit the lines that use FILLMEIN)
#endif

// LoRa Pins
#define LoRa_RST  14  // GPIO 14
#define LoRa_CS   18  // GPIO 18
#define LoRa_DIO0 26  // GPIO 26
#define LoRa_DIO1 33  // GPIO 33
#define LoRa_DIO2 32  // GPIO 32

// --hh
const int BUTTON_PIN = 36;
const int LED_RED_PIN = 17;
const int potPin = 37;
int button_state = 0;
int button_state_mem = 1;
int potValue = 0;

unsigned long time_now = 0;

// This EUI must be in little-endian format, so least-significant-byte
// first. When copying an EUI from ttnctl output, this means to reverse
// the bytes. For TTN issued EUIs the last bytes should be 0xD5, 0xB3,
// 0x70.
static const u1_t PROGMEM APPEUI[8]= { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 };
void os_getArtEui (u1_t* buf) { memcpy_P(buf, APPEUI, 8);}
 
// This should also be in little endian format, see above.
static const u1_t PROGMEM DEVEUI[8]= { 0x17, 0xCD, 0x05, 0xD0, 0x7E, 0xD5, 0xB3, 0x70 };
void os_getDevEui (u1_t* buf) { memcpy_P(buf, DEVEUI, 8);}
 
// This key should be in big endian format (or, since it is not really a
// number but a block of memory, endianness does not really apply). In
// practice, a key taken from ttnctl can be copied as-is.
// The key shown here is the semtech default key.
static const u1_t PROGMEM APPKEY[16] = { 0x02, 0x99, 0xA0, 0x4A, 0x61, 0xEC, 0xB2, 0xF2, 0x7B, 0x0B, 0xBC, 0x4C, 0xA5, 0xD1, 0x10, 0x41 };
void os_getDevKey (u1_t* buf) {  memcpy_P(buf, APPKEY, 16);}


#if 0
// This EUI must be in little-endian format, so least-significant-byte
// first. When copying an EUI from ttnctl output, this means to reverse
// the bytes. For TTN issued EUIs the last bytes should be 0xD5, 0xB3,
// 0x70.
static const u1_t PROGMEM APPEUI[8]={ FILLMEIN };
void os_getArtEui (u1_t* buf) { memcpy_P(buf, APPEUI, 8);}

// This should also be in little endian format, see above.
static const u1_t PROGMEM DEVEUI[8]={ FILLMEIN };
void os_getDevEui (u1_t* buf) { memcpy_P(buf, DEVEUI, 8);}

// This key should be in big endian format (or, since it is not really a
// number but a block of memory, endianness does not really apply). In
// practice, a key taken from ttnctl can be copied as-is.
static const u1_t PROGMEM APPKEY[16] = { FILLMEIN };
void os_getDevKey (u1_t* buf) {  memcpy_P(buf, APPKEY, 16);}
#endif

// --hh
void do_send(osjob_t* j);

static uint8_t mydata[5] = { 0, 0, 0, 0, 0 };
static osjob_t sendjob;

// Schedule TX every this many seconds (might become longer due to duty
// cycle limitations).
const unsigned TX_INTERVAL = 60;

// Pin mapping
const lmic_pinmap lmic_pins = {
    .nss = LoRa_CS,
    .rxtx = LMIC_UNUSED_PIN,
    .rst = LoRa_RST,
    .dio = { LoRa_DIO0, LoRa_DIO1, LoRa_DIO2 },
};


void showDatarate()
{
    switch (LMIC.datarate)
    {
        case DR_SF7:
            Serial.println(F("DR_SF7"));
            break;
 
        case DR_SF8:
            Serial.println(F("DR_SF8"));
            break;
 
        case DR_SF9:
            Serial.println(F("DR_SF9"));
            break;
 
        case DR_SF10:
            Serial.println(F("DR_SF10"));
            break;
 
        case DR_SF11:
            Serial.println(F("DR_SF11"));
            break;
 
        case DR_SF12:
            Serial.println(F("DR_SF12"));
            break;
    }
}


void showFrequency()
{
   Serial.print(F("LMIC.txChnl: "));
   Serial.println(LMIC.txChnl);
   char frequency[10];
   itoa(LMIC.freq, frequency, 10);
   Serial.print(F("freq: "));
   Serial.println(frequency);
}


void printHex2(unsigned v) {
    v &= 0xff;
    if (v < 16)
        Serial.print('0');
    Serial.print(v, HEX);
}

void onEvent (ev_t ev) {
    Serial.print(os_getTime());
    Serial.print(": ");
    switch(ev) {
        case EV_SCAN_TIMEOUT:
            Serial.println(F("EV_SCAN_TIMEOUT"));
            break;
        case EV_BEACON_FOUND:
            Serial.println(F("EV_BEACON_FOUND"));
            break;
        case EV_BEACON_MISSED:
            Serial.println(F("EV_BEACON_MISSED"));
            break;
        case EV_BEACON_TRACKED:
            Serial.println(F("EV_BEACON_TRACKED"));
            break;
        case EV_JOINING:
            Serial.println(F("EV_JOINING"));
            break;
        case EV_JOINED:
            Serial.println(F("EV_JOINED"));
            {
              u4_t netid = 0;
              devaddr_t devaddr = 0;
              u1_t nwkKey[16];
              u1_t artKey[16];
              LMIC_getSessionKeys(&netid, &devaddr, nwkKey, artKey);
              Serial.print("netid: ");
              Serial.println(netid, DEC);
              Serial.print("devaddr: ");
              Serial.println(devaddr, HEX);
              Serial.print("AppSKey: ");
              for (size_t i=0; i<sizeof(artKey); ++i) {
                if (i != 0)
                  Serial.print("-");
                printHex2(artKey[i]);
              }
              Serial.println("");
              Serial.print("NwkSKey: ");
              for (size_t i=0; i<sizeof(nwkKey); ++i) {
                      if (i != 0)
                              Serial.print("-");
                      printHex2(nwkKey[i]);
              }
              Serial.println();
            }
            // Disable link check validation (automatically enabled
            // during join, but because slow data rates change max TX
	    // size, we don't use it in this example.
            LMIC_setLinkCheckMode(0);
            break;
        /*
        || This event is defined but not used in the code. No
        || point in wasting codespace on it.
        ||
        || case EV_RFU1:
        ||     Serial.println(F("EV_RFU1"));
        ||     break;
        */
        case EV_JOIN_FAILED:
            Serial.println(F("EV_JOIN_FAILED"));
            break;
        case EV_REJOIN_FAILED:
            Serial.println(F("EV_REJOIN_FAILED"));
            break;
        case EV_TXCOMPLETE:
            mydata[0] = 0; 
            Serial.println(F("EV_TXCOMPLETE (includes waiting for RX windows)"));
            if (LMIC.txrxFlags & TXRX_ACK)
              Serial.println(F("Received ack"));
            if (LMIC.dataLen) {
              Serial.print(F("Received "));
              Serial.print(LMIC.dataLen);
              Serial.println(F(" bytes of payload: "));
              for (int i = LMIC.dataBeg; i < LMIC.dataBeg + LMIC.dataLen; i++) {
                  Serial.print(LMIC.frame[i], HEX);
                  Serial.print(" ");
              }
              digitalWrite(LED_RED_PIN, LMIC.frame[LMIC.dataBeg]&1); 
              Serial.print(" Port: ");
              Serial.print(LMIC.frame[LMIC.dataBeg-1]); // Port
              Serial.println(F(""));
            }
            // Schedule next transmission
            os_setTimedCallback(&sendjob, os_getTime() + sec2osticks(TX_INTERVAL), do_send);
            break;
        case EV_LOST_TSYNC:
            Serial.println(F("EV_LOST_TSYNC"));
            break;
        case EV_RESET:
            Serial.println(F("EV_RESET"));
            break;
        case EV_RXCOMPLETE:
            // data received in ping slot
            Serial.println(F("EV_RXCOMPLETE"));
            break;
        case EV_LINK_DEAD:
            Serial.println(F("EV_LINK_DEAD"));
            break;
        case EV_LINK_ALIVE:
            Serial.println(F("EV_LINK_ALIVE"));
            break;
        /*
        || This event is defined but not used in the code. No
        || point in wasting codespace on it.
        ||
        || case EV_SCAN_FOUND:
        ||    Serial.println(F("EV_SCAN_FOUND"));
        ||    break;
        */
        case EV_TXSTART:
            Serial.println(F("EV_TXSTART"));
            break;
        case EV_TXCANCELED:
            Serial.println(F("EV_TXCANCELED"));
            break;
        case EV_RXSTART:
            /* do not print anything -- it wrecks timing */
            break;
        case EV_JOIN_TXCOMPLETE:
            Serial.println(F("EV_JOIN_TXCOMPLETE: no JoinAccept"));
            break;

        default:
            Serial.print(F("Unknown event: "));
            Serial.println((unsigned) ev);
            break;
    }
}

void do_send(osjob_t* j){
    // Check if there is not a current TX/RX job running
    if (LMIC.opmode & OP_TXRXPEND) {
        Serial.println(F("OP_TXRXPEND, not sending"));
    } else {
        showDatarate();
        showFrequency();
        // Prepare upstream data transmission at the next possible time.
        LMIC_setTxData2(1, mydata, sizeof(mydata), 0);
        Serial.println(F("Packet queued"));
    }
    // Next TX is scheduled after TX_COMPLETE event.
}


void setup() {
    pinMode(LED_BUILTIN, OUTPUT);  /* weisse eingebaute LED */
    pinMode(LED_RED_PIN, OUTPUT);  /* rote externe LED */
    pinMode(BUTTON_PIN, INPUT );
    digitalWrite(LED_BUILTIN, LOW); 
    // digitalWrite(LED_RED_PIN, HIGH); 
    button_state = digitalRead(BUTTON_PIN);

    Serial.begin(115200);
    Serial.println(F("Starting"));

    // LMIC init
    os_init();
    // Reset the MAC state. Session and pending data transfers will be discarded.
    LMIC_reset();

    // Start job (sending automatically starts OTAA too)
    do_send(&sendjob);
}


#define PERIOD  200  /* msec */

void loop() {
    int pot = 0;

    // Hier nicht zu viel/zu langsamen Code einfuegen, da sonst os_runloop_once() 
    // ausgebremst wird.

    if (millis() >= time_now + PERIOD) {
        time_now += PERIOD;
        button_state = digitalRead(BUTTON_PIN);      
        if ((button_state == 0) && (button_state_mem == 1)) {
            Serial.println("Taste gedrueckt");
            button_state_mem = 0;
            // Kann in Konflikt kommen mit dem Loeschen von mydata in 
            // EV_TXCOMPLETE. Ist mir aber bei dieser Demo egal
            mydata[0] = 0x01;
        }
        else if ((button_state == 0) && (button_state_mem == 0)) {
            // button still pressed
        }
        else if ((button_state == 1) && (button_state_mem == 0)) {
            // button released
            button_state_mem = 1;
        }

        pot = analogRead(potPin); // 12-Bit default
        if ((pot >= potValue - 10) && (pot <= potValue + 10)) {
            /* pass */
        }
        else {
            Serial.print(F("ADC: ")); // ca. 160 ... 3920
            Serial.print(potValue);
            Serial.print(F(" (7-Bit: ")); 
            Serial.print(potValue >> 5);
            Serial.print(F(")")); 
        }
        potValue = pot;
        mydata[1] = potValue >> 5;  // 7-Bit ADC Value
    }

    os_runloop_once(); // ruft LMIC runloop processor auf
}
