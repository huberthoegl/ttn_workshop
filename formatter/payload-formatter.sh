# https://www.thethingsindustries.com/docs/integrations/payload-formatters/create/#create-an-application-payload-formatter-using-the-cli

# Muss vielleicht angepasst werden
appid=heltec-demo-1

ttn-lw-cli applications link set ${appid} \
--api-key $WORKSHOP_API_KEY \
--default-formatters.down-formatter FORMATTER_JAVASCRIPT \
--default-formatters.down-formatter-parameter-local-file "encoder.js" \
--default-formatters.up-formatter FORMATTER_JAVASCRIPT \
--default-formatters.up-formatter-parameter-local-file "decoder.js"
