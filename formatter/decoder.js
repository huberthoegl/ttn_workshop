// Uplink formatter (decoder.js)

function decodeUplink(input) {
  var adc = 0;
  var button = 0;
  button = input.bytes[0];
  adc = input.bytes[1];
  return {
    data: {
      adc: adc << 5,
      button: button
      // bytes: input.bytes,
    }
    // warnings: [],
    // errors: []
  };
}
