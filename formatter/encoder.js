// Downlink formatter (encoder.js)

// Called when a downlink with decoded payload is scheduled to be sent to
// device. The result is an encoded array of bytes.
// input is either { "led": "on" } or { "led": "off" }

var led_state = ["off", "on"];

function encodeDownlink(input) {
    var b = 0;
    if (input.data.led == "on") {
        b = 1
    }
    return {
        bytes: [b],
        fPort: 1,
        warnings: [],
        errors: []
    };
}

// Decodes the binary payload back to decoded payload.
function decodeDownlink(input) {
    return {
        data: {
            // bytes: input.bytes
            led: led_state[input.bytes[0]]
        },
        warnings: [],
        errors: []
    }
}

