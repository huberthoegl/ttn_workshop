# Beachten:
# - Umgebungsvariable WORKSHOP_API_KEY muss gesetzt sein
# - Applikations-ID muss angepasst werden

appid=heltec-demo-1

curl --location \
     --header 'Authorization: Bearer '$WORKSHOP_API_KEY \
     --header 'Content-Type: application/json' \
     --header 'User-Agent: my-integration/my-integration-version' \
     --request GET \
     https://eu1.cloud.thethings.network/api/v3/as/applications/${appid}/packages/storage/

