Base URL: https://webhook.site

Unique URL: https://webhook.site/a9242d1b-ebfb-42b4-b0ec-cd4f650437af
Unique E-mail Adress: 9242d1b-ebfb-42b4-b0ec-cd4f650437af@email.webhook.site
URL im Browser: https://webhook.site/#!/a9242d1b-ebfb-42b4-b0ec-cd4f650437af

In der Webhook Konfiguration traegt man bei Uplink message den Pfad 
/a9242d1b-ebfb-42b4-b0ec-cd4f650437af ein. Dann sieht man Uplink Daten 
wie folgt auf der Seite
https://webhook.site/a9242d1b-ebfb-42b4-b0ec-cd4f650437af:

```
{
  "end_device_ids": {
    "device_id": "eui-70b3d57ed005c674",
    "application_ids": {
      "application_id": "heltec-demo"
    },
    "dev_eui": "70B3D57ED005C674",
    "join_eui": "0000000000000000",
    "dev_addr": "260B6CCC"
  },
  "correlation_ids": [
    "as:up:01GYAED5ZC4GG2ZTAM4JGR7P3S",
    "gs:conn:01GY8Z67G3V31NQ7S735NBSBF5",
    "gs:up:host:01GY8Z67G7YGMRCY6EH2FYEYB9",
    "gs:uplink:01GYAED5RYTJ890VMD8VNXD1W1",
    "ns:uplink:01GYAED5RZMEG71BQFWHDFTDQV",
    "rpc:/ttn.lorawan.v3.GsNs/HandleUplink:01GYAED5RZN4RK086YMSV3X4PB",
    "rpc:/ttn.lorawan.v3.NsAs/HandleUplink:01GYAED5ZB9W1MXRKW5SQ8VB56"
  ],
  "received_at": "2023-04-18T15:05:52.875924299Z",
  "uplink_message": {
    "session_key_id": "AYeT8dic3opiznBDef2Rcw==",
    "f_port": 1,
    "f_cnt": 261,
    "frm_payload": "AAUAAAA=",
    "decoded_payload": {
      "adc": 160,
      "button": 0
    },
    "rx_metadata": [
      {
        "gateway_ids": {
          "gateway_id": "openiotf34",
          "eui": "B827EBFFFEAC3600"
        },
        "time": "2023-04-18T15:05:52.653590Z",
        "timestamp": 1318955820,
        "rssi": -74,
        "channel_rssi": -74,
        "snr": 6.5,
        "location": {
          "latitude": 48.36943,
          "longitude": 10.85343,
          "altitude": 481,
          "source": "SOURCE_REGISTRY"
        },
        "uplink_token": "ChgKFgoKb3BlbmlvdGYzNBIIuCfr//6sNgAQrNb29AQaDAjQ4/qhBhDKv+W/AiDgx5K/sYIM",
        "channel_index": 5,
        "received_at": "2023-04-18T15:05:52.670654410Z"
      }
    ],
    "settings": {
      "data_rate": {
        "lora": {
          "bandwidth": 125000,
          "spreading_factor": 7,
          "coding_rate": "4/5"
        }
      },
      "frequency": "867500000",
      "timestamp": 1318955820,
      "time": "2023-04-18T15:05:52.653590Z"
    },
    "received_at": "2023-04-18T15:05:52.671695484Z",
    "consumed_airtime": "0.051456s",
    "network_ids": {
      "net_id": "000013",
      "tenant_id": "ttn",
      "cluster_id": "eu1",
      "cluster_address": "eu1.cloud.thethings.network"
    }
  }
}
```
