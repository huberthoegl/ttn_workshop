
# compute/check base64 payload with "python base64tool.py -B <bytes>"
# AA== -> 0x00

# Muss vielleicht angepasst werden
appid=heltec-demo-1
devid=heltec-demo-dev1
webhookid=heltec-demo-wh

curl --location \
     --header 'Authorization: Bearer '$WORKSHOP_API_KEY \
     --header 'Content-Type: application/json' \
     --header 'User-Agent: my-integration/my-integration-version' \
     --request POST \
     --data '{"downlinks":[{ "frm_payload": "AA==", "f_port": 10, "priority":"NORMAL" }] }' \
     https://eu1.cloud.thethings.network/api/v3/as/applications/${appid}/webhooks/${webhookid}/devices/${devid}/down/replace

