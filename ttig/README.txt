hhoegl, 2023-04-17 

Hier ist die Prozedur für eine Neuinitialisierung des TTIG Gateways. 
Die angegebenen Daten (Wifi Pwd und Gateway-EUI) beziehen sich auf 
mein Gateway, diese Daten sollten bei Euch anders sein.

TTIG, 8 Kanaele, SX1308, ESP8266 WiFi, LoRaWAN 1.0.3, USB-C

1.  5 Sek Reset druecken (LED blinkt rot und gruen)

2. 10 Sek Setup druecken (LED blinkt dann schnell rot)
  
   Nun ist das TTIG neu initialisiert. Über einen integrierten AP 
   bietet es das Netz mit der SSID MiniHub-80453A an.

3. Rechner mit WiFi Netz MiniHub-80453A verbinden 

   - Firefox verwenden! 
   - http://192.168.4.1
   - Wifi Pwd: oJurJyfz

   Die Website des Accesspoints zeigt auch alle wichtigen Daten des 
   TTIG an, siehe den Screenshot TTIG-Daten.png.

4. Konfigurieren, so dass TTIG beim nächsten Starten als WiFi Client 
   im Heimnetz ist.

5. Reboot (gruen sollte irgendwann dauerhaft leuchten, dann ist alles richtig
   verbunden).  An der Fritzbox sieht man dann den Netzteilnehmer 
   MiniHub-80453A.

6. Gateway in der TTN Konsole eintragen:

   Gateway-EUI 58 A0 CB FF FE 80 45 3A

   Der Claim Authentication Code ist das Wifi Pwd.

