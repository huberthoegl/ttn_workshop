
'''
Compute base64 payload field from raw hex byte values e.g. for HTTP Downlink
Added reverse option to print bytes from base64 payload.

Sample downlink with "frm_payload" field:

curl --location \
     --header 'Authorization: Bearer '$WORKSHOP_API_KEY \
     --header 'Content-Type: application/json' \
     --header 'User-Agent: my-integration/my-integration-version' \
     --request POST \
     --data '{"downlinks":[{ "frm_payload": "AQM=", "f_port": 10, "priority":"NORMAL" }] }' \
     https://eu1.cloud.thethings.network/api/v3/as/applications/loris-base-test/webhooks/loriswh/devices/eui-7066e1fffe000d3a/down/replace

https://www.thethingsindustries.com/docs/integrations/webhooks/scheduling-downlinks/
https://www.thethingsindustries.com/docs/reference/data-formats/

Hubert.Hoegl@t-online.de, 2022-02-17, 2023-04-19
'''

import sys
import base64
import argparse

usage = '''
{pgm} -B 00
AA==
{pgm} -B 01
AQ==
{pgm} -B 1  3
AQM=
{pgm} -B 01 03
AQM=
{pgm} -B A7 C2 9F
p8Kf
{pgm} -b AAUAAAA=
0x00  0x05  0x00  0x00  0x00
'''.format(pgm=sys.argv[0])

parser = argparse.ArgumentParser(usage=usage)
parser.add_argument("-b", action="store_true", help="base64 to bytes")
parser.add_argument("-B", action="store_true", help="bytes to base64")
parser.add_argument('opts', nargs='+', help='options')

args = parser.parse_args()

if args.B:
    L = [int(i, 16) for i in args.opts]
    base64_bytes = bytes(L)
    base64_encoded_data = base64.b64encode(base64_bytes)
    print(base64_encoded_data.decode('ascii'))
if args.b:
    byts = base64.b64decode(args.opts[0])
    for b in byts:
        print("0x{:02x} ".format(b), end=" ")
    print()
