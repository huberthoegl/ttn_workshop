# TTN Workshop 

Der Begleittext ist unter

<https://hhoegl.de/ttn_workshop.html>

Damit die Beispiele funktionieren muss die Umgebungsvariable `WORKSHOP_API_KEY`
auf den API Key der Heltec-Demo Anwendung `heltec-demo-1` im TTN gesetzt werden.

Beispielkommando:

```
export WORKSHOP_API_KEY=NNSXS.3FY7WOZWTS4MUPW6NNBB3D6UOKQL4IJRPGSMLHQ.7T7BUZLPR23J2DGNV7X2M35IME34I2ITU5Q6DYQP42JRUXVZBKIA
```
