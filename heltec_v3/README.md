Demo fuer Heltec LoRa V3

hhoegl, 2024-07-12

Dieses Beispiel ist in der RadioLib enthalten: 
`RadioLib/examples/LoRaWAN/LoRaWAN_Starter`.
Ich habe nur kleinere Anpassungen vorgenommen.

----

Platformio installieren (ich habe mich an diese Anleitung 
gehalten:
<https://docs.platformio.org/en/latest/core/installation/methods/installer-script.html>)

```
curl -fsSL -o get-platformio.py https://raw.githubusercontent.com/platformio/platformio-core-installer/master/get-platformio.py
python3 get-platformio.py
```

Virtuelle Umgebung starten mit 

```
. ~/.platformio/penv/bin/activate
```


Projekt initialisieren

In `ttn_workshop/heltec_v3` ausführen:

```
pio project init --board heltec_wifi_lora_32_V3
```


Initialisieren (nur falls `.pio` Verzeichnis noch nicht existiert)

```
pio project init --board heltec_wifi_lora_32_V3
```


Kompilieren 

```
$ pio run 
```


Flashen (kompiliert auch, falls es Aenderungen gibt)

```
$ pio run -v -t upload
```

Es kann sein, dass man nach dem Flashen den Reset-Knopf auf dem Heltec
Board drücken muss.

Nach dem Flashen auf die serielle Schnittstelle schauen. Die LoRaWAN Demo 
schreibt Nachrichten mit `Serial.println()`.

```
$ picocom -b 115200 /dev/ttyUSB0 
```

Aus `picocom` kommt man heraus mit `Strg-A Strg-X`.

Sollte man einen `permission denied` Fehler bekommen, das bedeutet dass man 
als gewöhnlicher Benutzer nicht auf die serielle Schnittstelle kommt (sondern
nur der Administrator `root`), dann hilft folgende Kommandozeile:

```
sudo usermod -a -G dialout $USER
```

Danach muss man den Rechner neu booten, damit das Kommando wirksam wird.

