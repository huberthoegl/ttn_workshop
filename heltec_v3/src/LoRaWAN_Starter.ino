/*
  RadioLib LoRaWAN Starter Example

  This example joins a LoRaWAN network and will send
  uplink packets. Before you start, you will have to
  register your device at https://www.thethingsnetwork.org/
  After your device is registered, you can run this example.
  The device will join the network and start uploading data.

  Running this examples REQUIRES you to check "Resets DevNonces"
  on your LoRaWAN dashboard. Refer to the network's 
  documentation on how to do this.

  For default module settings, see the wiki page
  https://github.com/jgromes/RadioLib/wiki/Default-configuration

  For full API reference, see the GitHub Pages
  https://jgromes.github.io/RadioLib/

  For LoRaWAN details, see the wiki page
  https://github.com/jgromes/RadioLib/wiki/LoRaWAN

*/

#include <Arduino.h>
#include "config.h"


const int LED_RED_PIN = 7; /* J3, Pin 18 */
const int BUTTON_PIN = 6;  /* J3, Pin 17 */
const int ADC_CH0_PIN = 1; /* J3, Pin 12 */

int button_state;
int analog_value;
size_t downlinkSize;
uint8_t downlinkPayload[5];
uint8_t Port = 1;


void setup() {
  pinMode(LED_RED_PIN, OUTPUT);  /* rote externe LED, act. low */
  pinMode(LED_BUILTIN, OUTPUT);  /* weisse interne LED, act. high */ 
  pinMode(BUTTON_PIN, INPUT);

  for (int i=0; i<5; i++) {
      digitalWrite(LED_BUILTIN, LOW);
      digitalWrite(LED_RED_PIN, LOW);
      delay(100);
      digitalWrite(LED_BUILTIN, HIGH);
      digitalWrite(LED_RED_PIN, HIGH);
      delay(100);
  }

  Serial.begin(115200);
  while(!Serial);
  delay(5000);  // Give time to switch to the serial monitor
  Serial.println(F("\nSetup ... "));

  Serial.println(F("Initialise the radio"));
  int16_t state = radio.begin();
  debug(state != RADIOLIB_ERR_NONE, F("Initialise radio failed"), state, true);

  // Setup the OTAA session information
  node.beginOTAA(joinEUI, devEUI, nwkKey, appKey);

  Serial.println(F("Join ('login') the LoRaWAN Network"));
  state = node.activateOTAA();
  debug(state != RADIOLIB_LORAWAN_NEW_SESSION, F("Join failed"), state, true);

  Serial.println(F("Ready!\n"));
}

void loop() {
  uint8_t uplinkPayload[5] = {0, 0, 0, 0, 0};

  Serial.println(F("Sending uplink"));

  // This is the place to gather the sensor inputs
  // Instead of reading any real sensor, we just generate some random numbers as example
  // uint8_t value1 = radio.random(100);
  // uint16_t value2 = radio.random(2000);
  // uplinkPayload[0] = value1;
  // uplinkPayload[1] = highByte(value2);   // See notes for high/lowByte functions
  // uplinkPayload[2] = lowByte(value2);

  button_state = digitalRead(BUTTON_PIN);
  analog_value = analogRead(ADC_CH0_PIN); /* 12-bit ADC: 0 ... 4095 */
  Serial.print(F("Button state: "));
  Serial.println(button_state);
  Serial.print(F("Analog value: "));
  Serial.println(analog_value);
  uplinkPayload[0] = button_state;
  uplinkPayload[1] = analog_value >> 4;  /* convert to 0 ... 255 */  
  
  // Perform an uplink

  int16_t state = node.sendReceive(uplinkPayload, sizeof(uplinkPayload), Port, downlinkPayload, &downlinkSize);
  // int16_t state = node.sendReceive(uplinkPayload, sizeof(uplinkPayload));    

  Serial.println(stateDecode(state));
  debug((state != RADIOLIB_LORAWAN_NO_DOWNLINK) && (state != RADIOLIB_ERR_NONE), F("Error in sendReceive"), state, false);

   // Check if downlink was received
  if (state != RADIOLIB_LORAWAN_NO_DOWNLINK) {
    // Did we get a downlink with data for us
    if(downlinkSize > 0) {
      Serial.println(F("Downlink data: "));
      arrayDump(downlinkPayload, downlinkSize);
    } else {
      Serial.println(F("<MAC commands only>"));
    }
  }

  Serial.print(F("Uplink complete, next in "));
  Serial.print(uplinkIntervalSeconds);
  Serial.println(F(" seconds"));

  Serial.println("------");

    // Wait until next uplink - observing legal & TTN FUP constraints
  delay(uplinkIntervalSeconds * 1000UL);  // delay needs milli-seconds
}
