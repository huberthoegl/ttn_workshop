
# H. Hoegl, <Hubert.Hoegl@hs-augsburg.de>

import os, time

# pip install paho-mqtt
import paho.mqtt.client as mqtt


# =========== ANPASSEN ===================

# Application ID
user = "heltec-demo-1@ttn"

# End Device ID
device = "heltec-demo-dev1"

# ========================================

password = os.environ["WORKSHOP_API_KEY"]

topic = "v3/{}/devices/{}/up".format(user, device)

clientname = "heltec-client"

def on_connect(client, userdata, flags, rc):
    print("==== subscribing to", topic)
    client.subscribe(topic, qos=0)


def on_message(client, userdata, msg):
    print("\n++++ topic:", msg.topic)
    ps = msg.payload.decode('ascii')
    print(ps)


if __name__ == "__main__":
    client = mqtt.Client(clientname)
    client.on_connect = on_connect
    client.on_message = on_message
    client.username_pw_set(user, password)
    client.connect("eu1.cloud.thethings.network", port=1883)
    client.loop_start()   # event loop

    # end of main program would terminate event loop
    while True:
        time.sleep(10)

