# hhoegl, 2024-07-16
# taken from https://github.com/eclipse/paho.mqtt.python (README.md)

# pip install paho-mqtt
import paho.mqtt.client as mqtt

import os

# anpassen
user = "heltec-demo-v3-1@ttn"
device = "eui-70b3d57ed00690d8" 

password = os.environ["WORKSHOP_API_KEY"]

# The callback for when the client receives a CONNACK response from the server.
def on_connect(client, userdata, flags, reason_code, properties):
    print(f"Connected with result code {reason_code}")
    # Subscribing in on_connect() means that if we lose the connection and
    # reconnect then subscriptions will be renewed.
    client.subscribe("v3/{}/devices/{}/up".format(user, device))

# The callback for when a PUBLISH message is received from the server.
def on_message(client, userdata, msg):
    print(msg.topic+" "+str(msg.payload))

mqttc = mqtt.Client(mqtt.CallbackAPIVersion.VERSION2)
mqttc.username_pw_set(user, password)
mqttc.on_connect = on_connect
mqttc.on_message = on_message

mqttc.connect("eu1.cloud.thethings.network", 1883, 60)

# Blocking call that processes network traffic, dispatches callbacks and
# handles reconnecting.
# Other loop*() functions are available that give a threaded interface and a
# manual interface.
mqttc.loop_forever()



